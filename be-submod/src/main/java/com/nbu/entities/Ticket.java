package com.nbu.entities;

import com.nbu.commons.TravelClass;
import jakarta.persistence.*;

import java.math.BigDecimal;
import java.sql.ClientInfoStatus;

@Entity
@Table(name = "ticket")
public class Ticket {
    @Id
    @Column(name = "ticket_number")
    private String ticketNumber;

    @Column(name = "client_id")
    private Client clientID;

    @Column(name = "flight_number")
    private String flightNumber;

    @Column(name = "seat_number")
    private String seatNumber;

    @Column(name = "ticket_price")
    private BigDecimal price;

    @Column(name = "travel_class", nullable = false)
    @Enumerated(EnumType.STRING)
    private TravelClass travelClass;
//    private Luggage luggage;
}
