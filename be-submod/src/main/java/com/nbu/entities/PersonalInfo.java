package com.nbu.entities;

import com.nbu.commons.HealthStatus;
import jakarta.persistence.*;
import org.hibernate.annotations.CurrentTimestamp;

import java.time.LocalDate;

@Entity
@Table(name = "personal_info")
public class PersonalInfo {
    private String firstName;
    private String lastName;
    private HealthStatus healthStatus;
    private LocalDate birthDate;
    private String phoneNumber;

    @Id
    @JoinColumn(name = "id_number", nullable = false)
    @OneToOne(targetEntity = IdentityDocument.class, fetch = FetchType.LAZY)
    private String IdNumber;
}
