package com.nbu.entities;

import com.nbu.commons.Validations;
import jakarta.persistence.*;

@Entity
@Table(name = "airport")
public class Airport {
    @Id
    @Column(name = "airport_code")
    private String airportCode;

    @Column(name = "name", nullable = false)
    private String name;

    @ManyToOne(targetEntity = Country.class)
    @JoinColumn(name = "country_code", referencedColumnName = "code")
    private Country country;

    @ManyToOne(targetEntity = Country.class)
    @JoinColumn(name = "city_code", referencedColumnName = "code")
    private City city;

    public Airport(String airportCode, String name, Country country, City city) {
        Validations.threeLettersCode(airportCode);

        this.airportCode = airportCode;
        this.name = name;
        this.country = country;
        this.city = city;
    }

    public Airport() { }

    public String getAirportCode() {
        return airportCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }
}
