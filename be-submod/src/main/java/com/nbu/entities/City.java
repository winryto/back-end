package com.nbu.entities;

import com.nbu.commons.Validations;
import jakarta.persistence.*;

@Entity
@Table(name = "city")
public class City {
    @Id
    @Column(name = "city_code")
    private String code;

    @Column(name = "city_name", nullable = false)
    private String name;

    @ManyToOne(targetEntity = Country.class)
    @JoinColumn(name = "country_code", referencedColumnName = "code")
    private Country country;

    public City(String code, String name, Country country) {
        Validations.threeLettersCode(code);

        this.code = code;
        this.name = name;
        this.country = country;
    }

    public City() { }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
