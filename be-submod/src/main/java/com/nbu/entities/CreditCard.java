package com.nbu.entities;

import com.nbu.commons.CreditCardType;
import jakarta.persistence.*;

import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "creditCard")
public class CreditCard {
    @Column(name = "cardholder_name", nullable = false)
    private String cardholderName;

    @Id
    @Column(name = "card_number", nullable = false)
    private String cardNumber;

    @Column(name = "cvv", nullable = false)
    private String CVV;

    @Column(name = "expiration_date", nullable = false)
    private LocalDate expirationDate;

    @Column(name = "card_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private CreditCardType cardType;

    public CreditCard(String cardholderName, String cardNumber, String CVV, LocalDate expirationMonth, LocalDate expirationYear, CreditCardType cardType) {

        this.cardNumber = cardNumber;
        this.CVV = CVV;
        this.expirationDate = expirationMonth;
        this.cardType = cardType;
    }

    public CreditCard() { }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CreditCard)) return false;
        CreditCard that = (CreditCard) o;
        return cardNumber.equals(that.cardNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardNumber);
    }

    public CreditCardType getCardType() {
        return cardType;
    }

    public void setCardType(CreditCardType cardType) {
        this.cardType = cardType;
    }

    public String getCardholderName() {
        return cardholderName;
    }

    public void setCardholderName(String cardholderName) {
        this.cardholderName = cardholderName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCVV() {
        return CVV;
    }

    public void setCVV(String CVV) {
        this.CVV = CVV;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Override
    public String toString() {
        return  "Cardholder name: " + cardholderName + '\n' +
                "Card number: " + cardNumber + '\n' +
                "CVV: " + CVV + '\n' +
                "Expires on: " + expirationDate + '\n' +
                "Type: " + cardType.name();
    }
}




