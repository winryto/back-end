package com.nbu.entities;

import com.nbu.commons.HealthStatus;
import com.nbu.commons.Validations;
import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "country")
public class Country {
    @Id()
    @Column(name = "country_code")
    private String code;

    @Column(name = "country_name", nullable = false)
    private String name;

    @Column(name = "health_status")
    @Enumerated(EnumType.STRING)
    private HealthStatus healthStatus;

    public Country(String code, String name, HealthStatus healthStatus) {
        Validations.threeLettersCode(code);

        this.code = code;
        this.name = name;
        this.healthStatus = healthStatus;
    }

    public Country(String code, String name) {
        Validations.threeLettersCode(code);

        this.code = code;
        this.name = name;
    }

    public Country() {}

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public void setCode(String code) {
        Validations.threeLettersCode(code);
        
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HealthStatus getHealthStatus() {
        return healthStatus;
    }

    public void setHealthStatus(HealthStatus healthStatus) {
        this.healthStatus = healthStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Country country = (Country) o;
        return code.equals(country.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }
}
