package com.nbu.entities;

import com.nbu.commons.Validations;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import java.util.Objects;

@Entity
@Table(name = "airline")
public class Airline {
    @Id
    @Column(name = "airline_code")
    private String code;

    @Column(name = "name", nullable = false)
    private String name;

    public Airline(String code, String name) {
        Validations.threeLettersCode(code);

        this.code = code;
        this.name = name;
    }

    public Airline() {}

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Airline airline = (Airline) o;
        return code.equals(airline.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }
}
