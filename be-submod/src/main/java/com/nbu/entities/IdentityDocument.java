package com.nbu.entities;
import com.nbu.commons.DocumentType;
import com.nbu.commons.Sex;
import jakarta.persistence.*;

import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "identity_document")
public class IdentityDocument {

    @Column(name = "given_name", nullable = false)
    private String givenName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "birthdate", nullable = false)
    private LocalDate birthdate;

    @Column(name = "sex", nullable = false)
    @Enumerated(EnumType.STRING)
    private Sex sex;

    @Id
    @Column(name = "number", nullable = false)
    private String number;

    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    private DocumentType type;

    @Column(name = "citizenship", nullable = false)
    @OneToOne(targetEntity = Country.class, fetch = FetchType.LAZY)
    private Country citizenship;

    @Column(name = "issuing_country", nullable = false)
    @ManyToOne(targetEntity = Country.class, fetch = FetchType.LAZY)
    private Country issuingCountry;

    @Column(name = "expiration_date", nullable = false)
    private LocalDate expirationDate;


    public IdentityDocument(String givenName, String lastName, LocalDate birthdate, Sex sex, String number, DocumentType type, Country citizenship, Country issuingCountry, LocalDate expirityDate, LocalDate expirationDate) {
        this.givenName = givenName;
        this.lastName = lastName;
        this.birthdate = birthdate;
        this.sex = sex;
        this.number = number;
        this.type = type;
        this.citizenship = citizenship;
        this.issuingCountry = issuingCountry;
        this.expirationDate = expirationDate;
    }

    public IdentityDocument() {

    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public DocumentType getType() {
        return type;
    }

    public void setType(DocumentType type) {
        this.type = type;
    }

    public Country getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(Country citizenship) {
        this.citizenship = citizenship;
    }

    public Country getIssuingCountry() {
        return issuingCountry;
    }

    public void setIssuingCountry(Country issuingCountry) {
        this.issuingCountry = issuingCountry;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IdentityDocument)) return false;
        IdentityDocument that = (IdentityDocument) o;
        return number.equals(that.number) && issuingCountry.equals(that.issuingCountry);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, issuingCountry);
    }

    @Override
    public String toString() {
        return  "Given name(s): " + givenName + '\n' +
                "Last name: " + lastName + '\n' +
                "Birthdate:" + birthdate + '\n'+
                "Sex:" + sex + '\n' +
                "Document number: " + number + '\n' +
                "Document type:" + type + '\n' +
                "Citizenship: " + citizenship + '\n' +
                "Issuing country: " + issuingCountry + '\n';
    }
}
