package com.nbu.commons;

public enum Sex {
    MALE,
    FEMALE,
    NULL
}
