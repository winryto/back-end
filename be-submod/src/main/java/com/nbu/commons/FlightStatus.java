package com.nbu.commons;

public enum FlightStatus {
    LANDED,
    DEPARTED,
    EXPECTED,
    CANCELLED,
    NULL
}
