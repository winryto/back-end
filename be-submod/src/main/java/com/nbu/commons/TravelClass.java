package com.nbu.commons;

public enum TravelClass {
    FIRST,
    BUSINESS,
    ECONOMY,
    NULL
}
