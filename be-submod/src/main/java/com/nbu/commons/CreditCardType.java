package com.nbu.commons;

public enum CreditCardType {
    VISA,
    MASTERCARD,
    AMERICAN_EXPRESS,
    DISCOVER,
    NULL
}
