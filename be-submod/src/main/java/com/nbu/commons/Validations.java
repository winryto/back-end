package com.nbu.commons;

import java.time.LocalDate;

public final class Validations {

    private Validations(){}
    public static String airlineName(String airlineName){
        if (airlineName == null || airlineName.length() == 0) {
            throw new IllegalArgumentException("Name cannot be empty!");
        }
        return airlineName;
    }

    public static void threeLettersCode(String code){
        final String regex = "^[A-Z]{3}$";

        if (code == null || code.isEmpty()) {
            throw new NullPointerException("The identifier code should not be null or empty!");
        }
        if (!code.matches(regex)) {
            throw new IllegalArgumentException(Constants.ERR_INVALID_ARGUMENT + "code doesn't match regex " + regex);
        }
    }

    public static String airportName(String airportName){
        final String regex = "([A-Z][a-zA-Z ,.’-]+)([-]|[-][A-Z.-]?)([A-Z][a-zA-Z'-]+)";
        if (!airportName.matches(regex)){
            throw new IllegalArgumentException("Invalid airport code!");
        }
        return airportName;
    }

    public static String cityName(String cityName){
        final String regex = "^([a-zA-Z\\u0080-\\u024F]+(?:. |-| |'))*[a-zA-Z\\u0080-\\u024F]*$";
        if (!cityName.matches(regex)){
            throw new IllegalArgumentException("Invalid city name!");
        }
        return cityName;
    }
    public static String countryName(String countryName){
        final String regex = "[a-zA-Z]{2,}";
        if (!countryName.matches(regex)){
            throw new IllegalArgumentException("Invalid country name format!");
        }
        return countryName;
    }

    public static String creditCardNumber(String creditCardNumber){
        final String regex = "^[1-9][0-9]{15}$";
        if (!creditCardNumber.matches(regex)){
            throw new IllegalArgumentException("Invalid card number!");
        }
        return creditCardNumber;
    }

    public static String CVV(String CVV){
        final  String regex = "^[0-9]{3}$";
        if (!CVV.matches(regex)){
            throw new IllegalArgumentException("Invalid CVV!");
        }
        return CVV;
    }

    public static LocalDate expirationDate(LocalDate expirationDate){
        if (!expirationDate.isBefore(LocalDate.now())){
            throw new IllegalArgumentException("Expired!");
        }
        return expirationDate;
    }
}
