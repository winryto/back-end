package com.nbu.commons;

public abstract class Constants {
    public static final String ERR_INVALID_ARGUMENT = "Err: Invalid argument: ";
    public static final String CODE_NOT_EXIST = " with %d code does not exist!";
    public static final String NUMBER_NOT_EXIST = " with %d number does not exist!";
}
