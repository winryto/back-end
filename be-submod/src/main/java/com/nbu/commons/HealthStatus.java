package com.nbu.commons;

public enum HealthStatus {
    VACCINATED,
    NEGATIVE_PCR,
    NEGATIVE_ANTIGEN,
    NONE
}
