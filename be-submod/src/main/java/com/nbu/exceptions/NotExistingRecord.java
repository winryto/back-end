package com.nbu.exceptions;

public class NotExistingRecord extends RuntimeException {
    public NotExistingRecord(String message) {
        super(message);
    }
}
