package com.nbu.configuration;

import com.nbu.entities.*;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class SessionFactoryUtil {
    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            Configuration configuration = new Configuration();
            configuration.addAnnotatedClass(Airline.class);
            configuration.addAnnotatedClass(Airport.class);
            configuration.addAnnotatedClass(City.class);
            configuration.addAnnotatedClass(Country.class);
            configuration.addAnnotatedClass(CreditCard.class);
            configuration.addAnnotatedClass(Flight.class);
            configuration.addAnnotatedClass(Ticket.class);
            configuration.addAnnotatedClass(IdentityDocument.class);

            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties()).build();

            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        }

        return sessionFactory;
    }
}
