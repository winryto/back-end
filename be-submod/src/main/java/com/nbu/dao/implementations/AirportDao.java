package com.nbu.dao.implementations;

import com.nbu.configuration.SessionFactoryUtil;
import com.nbu.dao.Dao;
import com.nbu.entities.Airport;
import com.nbu.exceptions.NotExistingRecord;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.HashSet;
import java.util.Set;

import static com.nbu.commons.Constants.CODE_NOT_EXIST;

public class AirportDao implements Dao<Airport, String > {
    @Override
    public Airport get(String id) {
        Airport airport;
        try (Session session = SessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            airport = session.get(Airport.class, id);
            if (airport == null) {
                throw new NotExistingRecord(String.format("Airport" + CODE_NOT_EXIST, id));
            }
            transaction.commit();
        }
        return airport;
    }

    @Override
    public Set<Airport> getAll() {
        try (Session session = SessionFactoryUtil.getSessionFactory().openSession()) {
            return new HashSet<>(session.createQuery("SELECT airport FROM Airport airport", Airport.class).getResultList());
        }
    }

    @Override
    public void save(Airport object) {
        try (Session session = SessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            session.persist(object);
            transaction.commit();
        }
    }

    // TODO
    @Override
    public void update(Airport object, String[] changes) {

    }

    @Override
    public void delete(Airport object) {
        try (Session session = SessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            session.remove(object);
            transaction.commit();
        }
    }
}
