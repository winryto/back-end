package com.nbu.dao.implementations;

import com.nbu.configuration.SessionFactoryUtil;
import com.nbu.dao.Dao;
import com.nbu.entities.CreditCard;
import com.nbu.exceptions.NotExistingRecord;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.HashSet;
import java.util.Set;

import static com.nbu.commons.Constants.NUMBER_NOT_EXIST;

public class CreditCardDao implements Dao<CreditCard, String> {
    @Override
    public CreditCard get(String id) {
        CreditCard creditCard;
        try (Session session = SessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            creditCard = session.get(CreditCard.class, id);
            if (creditCard == null) {
                throw new NotExistingRecord(String.format("CreditCard" + NUMBER_NOT_EXIST, id));
            }
            transaction.commit();
        }
        return creditCard;
    }

    @Override
    public Set<CreditCard> getAll() {
        try (Session session = SessionFactoryUtil.getSessionFactory().openSession()) {
            return new HashSet<>(session.createQuery("SELECT creditCard FROM CreditCard creditCard", CreditCard.class).getResultList());
        }
    }

    @Override
    public void save(CreditCard object) {
        try (Session session = SessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            session.persist(object);
            transaction.commit();
        }
    }

    // TODO
    @Override
    public void update(CreditCard object, String[] changes) {

    }

    @Override
    public void delete(CreditCard object) {
        try (Session session = SessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            session.remove(object);
            transaction.commit();
        }
    }
}
