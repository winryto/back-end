package com.nbu.dao.implementations;

import com.nbu.configuration.SessionFactoryUtil;
import com.nbu.dao.Dao;
import com.nbu.entities.Airline;
import com.nbu.entities.Country;
import com.nbu.exceptions.NotExistingRecord;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.HashSet;
import java.util.Set;

import static com.nbu.commons.Constants.CODE_NOT_EXIST;

public class AirlineDao implements Dao<Airline, String> {

    @Override
    public Airline get(String id) {
        Airline airline;
        try (Session session = SessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            airline = session.get(Airline.class, id);
            if (airline == null) {
                throw new NotExistingRecord(String.format("Airline" + CODE_NOT_EXIST, id));
            }
            transaction.commit();
        }
        return airline;
    }

    @Override
    public Set<Airline> getAll() {
        try (Session session = SessionFactoryUtil.getSessionFactory().openSession()) {
            return new HashSet<>(session.createQuery("SELECT airline FROM Airline airline", Airline.class).getResultList());
        }
    }

    @Override
    public void save(Airline object) {
        try (Session session = SessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            session.persist(object);
            transaction.commit();
        }
    }

    // TODO
    @Override
    public void update(Airline object, String[] changes) {

    }

    @Override
    public void delete(Airline object) {
        try (Session session = SessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            session.remove(object);
            transaction.commit();
        }
    }
}
