package com.nbu.dao.implementations;

import com.nbu.configuration.SessionFactoryUtil;
import com.nbu.dao.Dao;
import com.nbu.entities.City;
import com.nbu.exceptions.NotExistingRecord;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.HashSet;
import java.util.Set;

import static com.nbu.commons.Constants.CODE_NOT_EXIST;

public class CityDao implements Dao<City, String> {
    @Override
    public City get(String id) {
        City city;
        try (Session session = SessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            city = session.get(City.class, id);
            if (city == null) {
                throw new NotExistingRecord(String.format("City" + CODE_NOT_EXIST, id));
            }
            transaction.commit();
        }
        return city;    }

    @Override
    public Set<City> getAll() {
        try (Session session = SessionFactoryUtil.getSessionFactory().openSession()) {
            return new HashSet<>(session.createQuery("SELECT city FROM City city", City.class).getResultList());
        }
    }

    @Override
    public void save(City object) {
        try (Session session = SessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            session.persist(object);
            transaction.commit();
        }
    }

    // TODO
    @Override
    public void update(City object, String[] changes) {

    }

    @Override
    public void delete(City object) {
        try (Session session = SessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            session.remove(object);
            transaction.commit();
        }
    }

    // TODO find all airports in this city?
}
