package com.nbu.dao.implementations;

import com.nbu.configuration.SessionFactoryUtil;
import com.nbu.dao.Dao;
import com.nbu.entities.IdentityDocument;
import com.nbu.exceptions.NotExistingRecord;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.HashSet;
import java.util.Set;

import static com.nbu.commons.Constants.NUMBER_NOT_EXIST;

public class IdentityDocumentDao implements Dao<IdentityDocument, String> {
    @Override
    public IdentityDocument get(String id) {
        IdentityDocument identityDocument;
        try (Session session = SessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            identityDocument = session.get(IdentityDocument.class, id);
            if (identityDocument == null) {
                throw new NotExistingRecord(String.format("IdentityDocument" + NUMBER_NOT_EXIST, id));
            }
            transaction.commit();
        }
        return identityDocument;
    }

    @Override
    public Set<IdentityDocument> getAll() {
        try (Session session = SessionFactoryUtil.getSessionFactory().openSession()) {
            return new HashSet<>(session.createQuery("SELECT identityDocument FROM IdentityDocument identityDocument", IdentityDocument.class).getResultList());
        }
    }

    @Override
    public void save(IdentityDocument object) {
        try (Session session = SessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            session.persist(object);
            transaction.commit();
        }
    }

    // TODO
    @Override
    public void update(IdentityDocument object, String[] changes) {

    }

    @Override
    public void delete(IdentityDocument object) {
        try (Session session = SessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            session.remove(object);
            transaction.commit();
        }
    }
}
