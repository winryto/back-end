package com.nbu.dao.implementations;

import com.nbu.configuration.SessionFactoryUtil;
import com.nbu.dao.Dao;
import com.nbu.entities.Country;
import com.nbu.exceptions.NotExistingRecord;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.HashSet;
import java.util.Set;

import static com.nbu.commons.Constants.CODE_NOT_EXIST;

public class CountryDao implements Dao<Country, String> {
    @Override
    public Country get(String id) {
        Country country;
        try (Session session = SessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            country = session.get(Country.class, id);
            if (country == null) {
                throw new NotExistingRecord(String.format("Country" + CODE_NOT_EXIST, id));
            }
            transaction.commit();
        }
        return country;
    }

    @Override
    public Set<Country> getAll() {
        try (Session session = SessionFactoryUtil.getSessionFactory().openSession()) {
            return new HashSet<>(session.createQuery("SELECT country FROM Country country", Country.class).getResultList());
        }
    }

    @Override
    public void save(Country object) {
        try (Session session = SessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            session.persist(object);
            transaction.commit();
        }
    }

    // TODO
    @Override
    public void update(Country object, String[] changes) {

    }

    @Override
    public void delete(Country object) {
        try (Session session = SessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            session.remove(object);
            transaction.commit();
        }
    }

    // TODO find all cities in given country
}
