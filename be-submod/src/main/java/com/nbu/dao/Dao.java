package com.nbu.dao;

import java.util.Set;

// T - object type
// I - identifier type
public interface Dao<T, I> {
    T get(I id);
    Set<T> getAll();
    void save(T object);
    void update(T object, String[] changes);
    void delete(T object);
}
