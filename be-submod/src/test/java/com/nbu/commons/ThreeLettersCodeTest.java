package com.nbu.commons;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

public class ThreeLettersCodeTest {
    @ParameterizedTest
    @NullAndEmptySource
    void nullOrEmptyCode(String input) {
        assertThrows(NullPointerException.class, () -> Validations.threeLettersCode(input));
    }

    @ParameterizedTest
    @ValueSource(strings = {"AR", "SOFI", "AUST"})
    void shorterOrLongerCode(String input) {
        assertThrows(IllegalArgumentException.class, () -> Validations.threeLettersCode(input));
    }

    @ParameterizedTest
    @ValueSource(strings = {"Bul", "sof", "LOn", "D N", " GB", "ES "})
    void notRegexMatching(String input) {
        assertThrows(IllegalArgumentException.class, () -> Validations.threeLettersCode(input));
    }

    @ParameterizedTest
    @ValueSource(strings = {"BUL", "SOF", "LON", "POL", "GBR", "ESP"})
    void validCodes(String input) {
        assertDoesNotThrow(() -> Validations.threeLettersCode(input));
    }
}
